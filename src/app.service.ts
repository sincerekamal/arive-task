import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return `Click <a href='/docs'>here</a> for API documentation`;
  }
}
