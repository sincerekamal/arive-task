import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common'
import { ObjectSchema } from 'joi'

@Injectable()
export class JoiValidationPipe implements PipeTransform {
  constructor (private schema: ObjectSchema) {}

  transform (value: any, metadata: ArgumentMetadata) {
    const { error, value: validatedPayload } = this.schema.validate(value, {
      allowUnknown: true,
    })
    if (error) {
      throw new BadRequestException(this.stringError(error))
    }

    return validatedPayload
  }

  stringError(error) {
    return error.details.map(e => e.message).join(', ') + '.';
  }
}
