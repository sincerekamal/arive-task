export enum PassionLevel {
  LOW = 'Low',
  MEDIUM = 'Medium',
  HIGH = 'High',
  VERY_HIGH = 'Very-High',
}

export const PassionLevelList = Object.values(PassionLevel)
