import * as Joi from 'joi'

export const createUpdateUserSchema = Joi.object({
  name: Joi.string().required().min(1).max(100)
});