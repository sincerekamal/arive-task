import * as Joi from 'joi'
import { PassionLevelList } from '../../../src/types/hobbies'

export const userHobbySchema = Joi.object({
  name: Joi.string()
    .required()
    .min(1)
    .max(100),
  year: Joi.number()
    .min(1950)
    .max(new Date().getFullYear()),
  passionLevel: Joi.string().valid(...PassionLevelList),
})
