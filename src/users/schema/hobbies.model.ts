import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose'
import { PassionLevel } from '../../../src/types/hobbies';

@Schema()
export class Hobby {
  @Prop()
  name: string

  @Prop()
  year: number

  @Prop()
  passionLevel: PassionLevel
}

export type HobbyDocument = Hobby & Document

export const HobbySchema = SchemaFactory.createForClass(Hobby)
