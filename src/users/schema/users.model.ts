import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import * as mongoose from 'mongoose'
import { Document } from 'mongoose'
import { Hobby } from './hobbies.model'

@Schema()
export class User {
  @Prop()
  name: string

  @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Hobby' }] })
  hobbies: any[]
}

export type UserDocument = User & Document

export const UserSchema = SchemaFactory.createForClass(User)
