import { Test, TestingModule } from '@nestjs/testing'
import { CreateUserDto } from './dto/create-user.dto'
import { UsersController } from './users.controller'
import { UsersService } from './users.service'

jest.mock('./users.service')

describe('UsersController', () => {
  let controller: UsersController
  let service: UsersService

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [UsersService],
    }).compile()

    controller = module.get<UsersController>(UsersController)
    service = module.get<UsersService>(UsersService)
  })

  it('should be defined', () => {
    expect(controller).toBeDefined()
  })

  describe(`create user > `, () => {
    it(`should call service.createUser()`, async () => {
      (service.createUser as jest.Mock).mockReturnValue({ id: 'objectid' })
      const payload = {} as CreateUserDto

      await controller.create(payload)

      expect(service.createUser).toHaveBeenCalledWith(payload)
    })

    it(`should return the user id in the proper response structure`, async () => {
      (service.createUser as jest.Mock).mockReturnValue({ id: 'objectid' })
      const payload = {} as CreateUserDto

      const expected = await controller.create(payload)

      expect(expected).toStrictEqual({
        success: true,
        data: {
          id: 'objectid',
        },
        message: 'User created successfully',
      })
    })
  })
})
