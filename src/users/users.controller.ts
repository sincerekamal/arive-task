import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common'
import { UsersService } from './users.service'
import { CreateUserDto } from './dto/create-user.dto'
import { UpdateUserDto } from './dto/update-user.dto'
import { JoiValidationPipe } from '../../src/joi-validation.pipe'
import { createUpdateUserSchema } from './joi-schema/create-update-user.schema'
import { userHobbySchema } from './joi-schema/user-hobby.schema'

@Controller('users')
export class UsersController {
  constructor (private readonly usersService: UsersService) {}

  @Post()
  async create (
    @Body(new JoiValidationPipe(createUpdateUserSchema))
    createUserDto: CreateUserDto,
  ) {
    const user = await this.usersService.createUser(createUserDto)
    return {
      message: 'User created successfully',
      success: true,
      data: {
        id: user.id,
      },
    }
  }

  @Get()
  async findAll () {
    const users = await this.usersService.findUsers()
    const message = users.length
      ? 'User(s) fetched successfully'
      : 'No users found'
    return {
      data: users,
      success: true,
      message,
    }
  }

  @Get(':id')
  async findOne (@Param('id') id: string) {
    const user = await this.usersService.findUser(id)

    return {
      data: user,
      success: true,
      message: 'User fetched successfully',
    }
  }

  @Patch(':id')
  async update (
    @Param('id') id: string,
    @Body(new JoiValidationPipe(createUpdateUserSchema))
    updateUserDto: UpdateUserDto,
  ) {
    const user = await this.usersService.updateUser(id, updateUserDto)
    
    return {
      data: { id: user.id },
      success: true,
      message: 'User updated successfully',
    }
  }

  @Delete(':id')
  async remove (@Param('id') id: string) {
    const user = await this.usersService.removeUser(id)
    
    return {
      data: { id: user.id },
      success: true,
      message: 'User updated successfully',
    }
  }

  @Post(':userId/hobbies')
  async addHobby (
    @Param('userId') id: string,
    @Body(new JoiValidationPipe(userHobbySchema)) payload,
  ) {
    const result = await this.usersService.addHobby(id, payload)
    return {
      success: true,
      message: 'Hobby added successfully',
      data: {
        id: result.id,
      },
    }
  }

  @Get(':userId/hobbies')
  async getUserHobbies (@Param('userId') userId: string) {
    const hobbies = await this.usersService.getHobbies(userId)
    return {
      success: true,
      message: "User's hobbies fetched successfully",
      data: hobbies,
    }
  }

  @Delete(':userId/hobbies/:id')
  async deleteHobby (@Param('userId') userId: string, @Param('id') hobbyId: string) {
    const result = await this.usersService.deleteHobby(userId, hobbyId)
    return {
      success: true,
      message: 'Hobby deleted successfully',
      data: {
        id: result.id,
      },
    }
  }
}
