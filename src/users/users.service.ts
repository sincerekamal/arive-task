import {
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'
import { CreateUserDto } from './dto/create-user.dto'
import { UpdateUserDto } from './dto/update-user.dto'
import { Hobby, HobbyDocument } from './schema/hobbies.model'
import { User, UserDocument } from './schema/users.model'

@Injectable()
export class UsersService {
  constructor (
    @InjectModel(User.name) private userModel: Model<UserDocument>,
    @InjectModel(Hobby.name) private hobbyModel: Model<HobbyDocument>,
  ) {}

  createUser (createUserDto: CreateUserDto) {
    return this.userModel.create(createUserDto)
  }

  findUsers () {
    return this.userModel.find({})
  }

  async findUser (id: string) {
    const user = await this.userModel.findOne({ _id: id }).populate('hobbies')

    if (!user) {
      throw new NotFoundException('User not found')
    }

    return user
  }

  async updateUser (id: string, updateUserDto: UpdateUserDto) {
    const user = await this.findUser(id)
    user.name = updateUserDto.name
    return user.save()
  }

  async removeUser (id: string) {
    const user = await this.findUser(id)
    return user.delete()
  }

  async addHobby (userId: string, payload) {
    await this.findUser(userId)

    const hobby = await this.hobbyModel.create(payload)

    await this.userModel.updateOne(
      { _id: userId },
      {
        $push: { hobbies: hobby.id },
      },
    )

    return hobby
  }

  async getHobbies (userId: string) {
    const user = await this.findUser(userId)

    return user.hobbies
  }

  async deleteHobby (userId: string, hobbyId: string) {
    const user = await this.findUser(userId)

    // check if user has this hobby
    const hobbyIndex = user.hobbies.findIndex(h => h.id === hobbyId)

    if (hobbyIndex < 0) {
      throw new ForbiddenException(`User doesn't have this hobby`)
    }

    const hobby = await this.hobbyModel.findByIdAndDelete(hobbyId)

    if (!hobby) {
      throw new NotFoundException('Hobby not found')
    }

    user.hobbies.splice(hobbyIndex, 1)

    await user.save()

    return hobby
  }
}
